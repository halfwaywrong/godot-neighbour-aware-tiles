*Godot Neighbour Aware Tiles*

To run, simply open the project in Godot. Click on a tile to remove it, or an empty tile to place a new one.

Credits
Originally conceived as a port of the [Neighbour Aware Tile Selection tutorial](http://www.saltgames.com/article/awareTiles/) by [Sam Driver of Salt Games](http://www.saltgames.com/). Also with a little help from [Neil Knauth](https://neilmakesgames.tumblr.com/)'s [Love2D/Lua example]( https://bitbucket.org/neilmakesgames/neighbour-aware-tile-selection-in-lua).

Tiles are by [Agnes Heyer](http://www.retinaleclipse.com/) (accessed via [this forum post](https://forums.tigsource.com/index.php?topic=14166.0)), under a [Creative Commons BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/) licence.

License
[Creative Commons BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/).