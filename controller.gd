
extends Node2D

var map
var last_mouse_down = false

var ev = InputEvent()

func _ready():
	map = get_node("../TileMap")
	set_process_input(true)
	
	ev.type = InputEvent.MOUSE_BUTTON
	ev.button_index = BUTTON_LEFT
	


func _input(ev):
	if ev.is_pressed():
		map._select_cell(ev.global_pos)

