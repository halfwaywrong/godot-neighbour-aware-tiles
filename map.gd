extends TileMap

export var tilesize = 16

var _tileset

func _ready():
	
	randomize()
	
	_setup_map()
	_setup_tiles()


func _setup_map():
	var x = 0
	var y = 0
	
	while x < get_viewport().get_rect().size.x / tilesize :
		while y < get_viewport().get_rect().size.y / tilesize:
			if randi() % 5 == 1:
				set_cell(x, y, 0)
			y += 1
		y = 0
		x += 1

func _setup_tiles():
	var x = 0
	var y = 0
	
	while x < get_viewport().get_rect().size.x / tilesize :
		while y < get_viewport().get_rect().size.y / tilesize:
			if get_cell(x, y) >= 0:
				set_cell(x, y, _get_tile_neighbours(x, y))
			y += 1
			
		y = 0
		x += 1

func _select_cell(pos):
	pos = world_to_map(pos)
	
	if get_cell(pos.x, pos.y) >= 0:
		set_cell(pos.x, pos.y, -1)
	else:
		set_cell(pos.x, pos.y, _get_tile_neighbours(pos.x, pos.y))
	
	if get_cell(pos.x + 1, pos.y) >= 0:
		set_cell(pos.x + 1, pos.y, _get_tile_neighbours(pos.x + 1, pos.y))
	if get_cell(pos.x - 1, pos.y) >= 0:
		set_cell(pos.x - 1, pos.y, _get_tile_neighbours(pos.x - 1, pos.y))
	if get_cell(pos.x, pos.y + 1) >= 0:
		set_cell(pos.x, pos.y + 1, _get_tile_neighbours(pos.x, pos.y + 1))
	if get_cell(pos.x, pos.y - 1) >= 0:
		set_cell(pos.x, pos.y - 1, _get_tile_neighbours(pos.x, pos.y - 1))


func _get_tile_neighbours(x, y):

	var sum = 0
	if get_cell(x, y - 1) >= 0:
		sum += 1
	if get_cell(x - 1, y) >= 0:
		sum += 2
	if get_cell(x, y + 1) >= 0:
		sum += 4
	if get_cell(x + 1, y) >= 0:
		sum += 8
	return sum

